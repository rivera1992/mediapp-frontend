import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/_service/login.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  roles:string[]=[];
  username:string;

  constructor(
    private loginService:LoginService
  ) { }

  ngOnInit(): void {
   var result = this.loginService.obtenerUser();
   this.username = result.user_name;
   this.roles = result.authorities;
  }

}
