import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { map,Observable } from 'rxjs';
import { Paciente } from 'src/app/_model/paciente';
import { Signos } from 'src/app/_model/Signos';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignosService } from 'src/app/_service/signos.service';


@Component({
  selector: 'app-signos',
  templateUrl: './signos.component.html',
  styleUrls: ['./signos.component.css']
})
export class SignosComponent implements OnInit {
  displayedColumns: string[] = ['idSignos', 'temperatura', 'pulso', 'ritmo','fecha','paciente','acciones'];

  dataSource = new MatTableDataSource<any>();
  cantidad:number;
  listarPaciente$:Observable<Paciente[]>;
  pacientesFiltrados$:Observable<Paciente[]>;
  pacienteSeleccionado:Paciente;
  pacienteFormControl=new FormControl();
  pacientes:Paciente[];
  signo?:Signos=new Signos();
  hide:boolean=false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private signosService:SignosService,
    private pacienteService:PacienteService
  ) { }

  ngOnInit(): void {

    this.signosService.listarSintomas(0,10).subscribe(data =>{
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
    });

    this.signosService.getSignosCambio().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);    
      this.dataSource.paginator = this.paginator;
    });


  }


  mostrarMas(e: any){
    this.signosService.listarSintomas(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
    });
  }

 eliminar(id:number){
  this.signosService.eliminar(id).subscribe(data=>{
    this.signosService.listarSintomas(0,10).subscribe(data => {
      this.signosService.setSignosCambio(data.content);
    });
  });
 }

 mostrarEdicion(){
   this.router.navigate(['/pages/signos/nuevo']);
 }
 validateRoute(){
   return this.route.children.length!=0;
 }
}
