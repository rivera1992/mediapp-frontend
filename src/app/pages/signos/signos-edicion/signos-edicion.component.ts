import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import {map, Observable } from 'rxjs';
import { Paciente } from 'src/app/_model/paciente';
import { Signos } from 'src/app/_model/Signos';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignosService } from 'src/app/_service/signos.service';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  listarPaciente$:Observable<Paciente[]>;
  pacientesFiltrados$:Observable<Paciente[]>;
  pacienteSeleccionado:Paciente;
  pacienteFormControl=new FormControl();
  pacientes:Paciente[];
  signo?:Signos=new Signos();
  hide:boolean=false;
  idSigno:number;


  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private signosService:SignosService,
    private pacienteService:PacienteService
  ) { }

  ngOnInit(): void {

    this.idSigno = parseInt(this.route.snapshot.paramMap.get("id"));

    if(this.idSigno){      
      this.signosService.listarPorId(this.idSigno).subscribe(data=>{
        this.signo = data;
      });
    }

    this.pacienteService.listar().subscribe(data=>{
      this.pacientes=data;
      this.listarPaciente$ = this.pacienteFormControl.valueChanges.pipe(map(val => this._filter(val)));
    
    });
    
  }

  mostrarPaciente(value:Paciente){
    return value?.nombres;    
  }

  private _filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(el =>
        el.nombres?.toLowerCase().includes(val.nombres?.toLowerCase()) || el.apellidos?.toLowerCase().includes(val?.apellidos.toLowerCase()) 
      );
    }
    return this.pacientes.filter(el =>
      el.nombres?.toLowerCase().includes(val?.toLowerCase()) || el.apellidos?.toLowerCase().includes(val?.toLowerCase()) 
    );
  }

  guardar(){
    this.signo.fecha=moment(this.signo.fecha).format('YYYY-MM-DDTHH:mm:ss');
    this.signosService.registrar(this.signo).subscribe(data=>{
      this.signosService.listarSintomas(0,10).subscribe(data => {
        this.signosService.setSignosCambio(data.content);
      });
      this.router.navigate(["/pages/signos"]);
    });
   
   // this.limpiarControler();
  }
  limpiarControler(){
    this.signo=new Signos();
  };

}
