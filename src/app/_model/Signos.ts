import { Paciente } from "./paciente";

export class Signos{
    idSigno:number;
    temperatura:string;
    pulso:string;
    ritmo:string;
    paciente:Paciente;
    fecha:string;
}